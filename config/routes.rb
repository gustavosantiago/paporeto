Paporeto::Application.routes.draw do
  devise_for :users
  resources :categories, except: :show
  
  resources :articles
  resources :users, except: :show

  root 'articles#index'
end 